# Telegram task bot

Task manager in telegram bot with heroku and telegram bot api

### Prerequisites

```
install golang: https://golang.org/doc/install
```

### Getting Started

open dialog with bot in telegram https://t.me/CheenasTaskBot

write one of this commands:
* `/tasks`
* `/new XXX YYY ZZZ` - creates a new task
* `/assign_$ID` - assigns a task to the user
* `/unassign_$ID` - removes a task from the current user
* `/resolve_$ID` - resolves a task, removes it from the task list
* `/my` - shows the tasks that are assigned to me
* `/owner` - shows the tasks that were created by me

### Running the tests

```
go test -v -mod=vendor
```