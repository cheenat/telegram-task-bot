package main

// сюда писать код

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"

	// tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	tgbotapi "gopkg.in/telegram-bot-api.v4" // for running tests
)

type Task struct {
	ID          int
	Creator     int64
	Executor    int64
	Description string
}

type Repo struct {
	Tasks map[int]*Task
	Users map[int64]string
}

var (
	// @BotFather в телеграме даст вам это
	// BotToken = "removed for security reasons"

	// урл выдаст вам игрок или хероку
	// WebhookURL = "https://cheenas-task-bot.herokuapp.com/"

	repo              = &Repo{Tasks: map[int]*Task{}, Users: map[int64]string{}}
	createNewTaskFunc = repo.newTaskClosure()
)

func (r Repo) showSpecificTasks(chatID int64, taskType string) string {
	if taskType == "tasks" && len(r.Tasks) == 0 {
		return "Нет задач"
	}
	answer := []string{}
	for taskID, task := range r.Tasks {
		res := ""
		taskIDStr := strconv.Itoa(taskID)
		if task.Creator != chatID && taskType == "owner" || task.Executor != chatID && taskType == "my" {
			continue
		}
		if task.Executor == chatID {
			if taskType == "tasks" {
				res = "\nassignee: я\n/unassign_" + taskIDStr + " /resolve_" + taskIDStr
			} else {
				res = "\n/unassign_" + taskIDStr + " /resolve_" + taskIDStr
			}
		} else if task.Executor != 0 {
			if taskType == "tasks" {
				res = "\nassignee: @" + r.Users[task.Executor]
			}
		} else {
			res = "\n/assign_" + taskIDStr
		}
		answer = append(answer, taskIDStr+". "+task.Description+" by @"+r.Users[task.Creator]+res)
	}
	if len(answer) == 0 && taskType != "tasks" {
		return "Нет задач"
	}
	sort.Strings(answer)
	return strings.Join(answer, "\n\n")
}

func (r *Repo) newTaskClosure() func(string, int64) string {
	id := 0
	return func(task string, creator int64) string {
		if task == "" {
			return "Задача не указана"
		}
		id += 1
		r.Tasks[id] = &Task{ID: id, Creator: creator, Description: task}
		return `Задача "` + task + `" создана, id=` + strconv.Itoa(id)
	}
}

func (r *Repo) editTask(taskID int, chatID int64, editType string) map[int64]string {
	answers := map[int64]string{}
	messageToCreator := ""
	if _, ok := r.Tasks[taskID]; !ok {
		if taskID == -1 {
			answers[chatID] = "id задачи не указан"
		} else {
			answers[chatID] = "нет задачи с id=" + strconv.Itoa(taskID)
		}
		return answers
	}
	if r.Tasks[taskID].Executor != chatID && editType != "assign" {
		answers[chatID] = "Задача не на вас"
		return answers
	} else if editType == "assign" {

	}
	if editType == "unassign" {
		answers[chatID] = "Принято"
		messageToCreator = `Задача "` + r.Tasks[taskID].Description + `" осталась без исполнителя`
	} else if editType == "resolve" {
		answers[chatID] = `Задача "` + r.Tasks[taskID].Description + `" выполнена`
		messageToCreator = `Задача "` + r.Tasks[taskID].Description + `" выполнена @` + r.Users[chatID]
	} else if editType == "assign" {
		if r.Tasks[taskID].Executor != 0 {
			answers[r.Tasks[taskID].Executor] = `Задача "` + r.Tasks[taskID].Description + `" назначена на @` + r.Users[chatID]
		} else {
			answers[r.Tasks[taskID].Creator] = `Задача "` + r.Tasks[taskID].Description + `" назначена на @` + r.Users[chatID]
		}
	}

	if r.Tasks[taskID].Creator != chatID && editType != "assign" {
		answers[r.Tasks[taskID].Creator] = messageToCreator
	}
	if editType == "unassign" {
		r.Tasks[taskID].Executor = 0
	} else if editType == "resolve" {
		delete(r.Tasks, taskID)
	} else if editType == "assign" {
		r.Tasks[taskID].Executor = chatID
		answers[chatID] = `Задача "` + r.Tasks[taskID].Description + `" назначена на вас`
	}
	return answers
}

func startTaskBot(ctx context.Context) error {
	// сюда пишите ваш код
	bot, err := tgbotapi.NewBotAPI(BotToken)
	if err != nil {
		log.Fatalf("NewBotAPI failed: %s", err)
	}
	bot.Debug = true
	fmt.Printf("Authorized on account %s\n", bot.Self.UserName)
	_, err = bot.SetWebhook(tgbotapi.NewWebhook(WebhookURL))
	if err != nil {
		log.Fatalf("SetWebhook failed: %s", err)
	}
	updates := bot.ListenForWebhook("/")

	http.HandleFunc("/state", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("all is working"))
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	go func() {
		log.Fatalln("http err:", http.ListenAndServe(":"+port, nil))
	}()
	fmt.Println("start listen :" + port)
	commandRgx, err := regexp.Compile("([a-z]+)")
	if err != nil {
		log.Printf("Failed to compile command regexp %v\n", err)
		return err
	}
	taskIDRgx, err := regexp.Compile("([0-9]+)")
	if err != nil {
		log.Printf("Failed to compile taskID regexp %v\n", err)
		return err
	}
	for update := range updates {
		if update.Message == nil {
			continue
		}
		chatID := update.Message.Chat.ID
		messageText := update.Message.Text
		answer := ""
		if _, ok := repo.Users[chatID]; !ok {
			repo.Users[chatID] = update.Message.Chat.UserName
		}
		switch commandRgx.FindString(messageText) {
		case "tasks", "new", "my", "owner":
			if commandRgx.FindString(messageText) == "new" {
				answer = createNewTaskFunc(strings.Join(strings.Split(messageText, " ")[1:], " "), chatID)
			} else {
				answer = repo.showSpecificTasks(chatID, commandRgx.FindString(messageText))
			}

			_, err := bot.Send(tgbotapi.NewMessage(
				chatID,
				answer,
			))
			if err != nil {
				log.Printf("Failed to send message %v, it's normaml if you running tests\n", err)
				continue
			}
		case "assign", "unassign", "resolve":
			var taskID int
			if taskIDRgx.FindStringSubmatch(messageText) == nil {
				taskID = -1
			} else {
				taskID, err = strconv.Atoi(taskIDRgx.FindString(messageText))
				if err != nil {
					log.Printf("Failed to convert taskID regexp from string to int %v\n", err)
					continue
				}
			}
			answers := map[int64]string{}

			answers = repo.editTask(taskID, chatID, commandRgx.FindString(messageText))
			for chatID, answer := range answers {
				_, err := bot.Send(tgbotapi.NewMessage(
					chatID,
					answer,
				))
				if err != nil {
					log.Printf("Failed to send message %v, it's normaml if you running tests\n", err)
					continue
				}
			}
		default:
			_, err := bot.Send(tgbotapi.NewMessage(
				chatID,
				"unknown command",
			))
			if err != nil {
				log.Printf("Failed to send message %v, it's normaml if you running tests\n", err)
				continue
			}
		}
	}
	return nil
}

func main() {
	err := startTaskBot(context.Background())
	if err != nil {
		panic(err)
	}
}
